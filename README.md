# Purpose:

This project demonstrates how to integrate only authentication part into you react project using existing AWS Cognito user pool.
In here we will explore how to configure Amplify and use the custom UI to implement login system.

If in case you don't have user pool setup on AWS Cognito. Please refer following steps to create one.

# Cognito Attributes Configuration:

![cognito-configuration-MFA](./artifacts/cognito-configuration-attributes-1.png?raw=true)

# Cognito Policies Configuration:

![cognito-configuration-MFA](./artifacts/cognito-configuration-policies-2.png?raw=true)

# Cognito MFA Configuration:

![cognito-configuration-MFA](./artifacts/cognito-configuration-MFA-3.png?raw=true)

# Cognito Messages And Mails Settings:

![cognito-configuration-MFA](./artifacts/cognito-configuration-message-4.png?raw=true)

# Cognito App Client Creation:

![cognito-configuration-MFA](./artifacts/cognito-configuration-appClient-5.png?raw=true)

# Cognito App Client Settings:

![cognito-configuration-MFA](./artifacts/cognito-configuration-appClient-settings-6.png?raw=true)
