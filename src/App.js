import logo from './logo.svg';
import './App.css';
import React from 'react'
import Amplify, { Auth, Authenticator } from 'aws-amplify';
import { AmplifySignOut, withAuthenticator, AmplifyAuthenticator, AmplifySignUp, } from '@aws-amplify/ui-react'
import { AuthState, onAuthUIStateChange } from '@aws-amplify/ui-components'

Amplify.configure({
  Auth: {
    // REQUIRED - Amazon Cognito Region
    region: 'ap-south-1',
    // OPTIONAL - Amazon Cognito Federated Identity Pool Region 
    // Required only if it's different from Amazon Cognito Region
    identityPoolRegion: 'ap-south-1',
    // OPTIONAL - Amazon Cognito User Pool ID
    userPoolId: 'ap-south-XXXXXXX',
    // OPTIONAL - Amazon Cognito Web Client ID (26-char alphanumeric string)
    userPoolWebClientId: '7kXXXXXXXXXXXXXXXp3',
    // OPTIONAL - Enforce user authentication prior to accessing AWS resources or not
    mandatorySignIn: false,
    // OPTIONAL - Manually set the authentication flow type. Default is 'USER_SRP_AUTH'
    authenticationFlowType: 'USER_PASSWORD_AUTH',
    // OPTIONAL - Hosted UI configuration
    oauth: {
      domain: 'your-setup-domain',
      scope: ['email', 'openid'],
      redirectSignIn: 'http://localhost:3000/',
      redirectSignOut: 'http://localhost:3000/',
      responseType: 'code'
    }
  }
});

const authReducer = (state, action) => {
  switch (action.type) {
    case 'authStateChange':
      return { authStage: action.authStage, user: action.user }
    default:
      throw Error(`action ${action.type} not found.`)
  }
}

const initialState = {}

function App() {

  const [state, dispatch] = React.useReducer(authReducer, initialState)

  React.useEffect(() => {
    //this will fire anytime a user switches auth scenarios
    // https://docs.amplify.aws/ui/auth/authenticator/q/framework/react#methods--enums
    onAuthUIStateChange((nextAuthState, data) => {
      dispatch({
        type: 'authStateChange',
        authStage: nextAuthState,
        user: data,
      })
    })
  }, [])

  return state.authStage === AuthState.SignedIn && state.user ? (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Yey !! you are logged in :)
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <AmplifySignOut />
      </header>
    </div>
  ) : (
    <AmplifyAuthenticator usernameAlias="email">
      <AmplifySignUp
        slot="sign-up"
        usernameAlias="email"
        formFields={[
          { key: 'given_name', label: 'Given Name', required: true, placeholder: 'First name and last name', type: 'given_name', displayOrder: 1 },
          { key: 'email', label: 'Email Id', required: true, placeholder: 'Email Id', type: 'email', displayOrder: 2 },
          { key: 'gender', label: 'Gender', required: true, placeholder: 'Male or Female', type: 'gender', displayOrder: 3 },
          { key: 'birthdate', label: 'Birthdate', required: true, placeholder: 'mm/dd/yyyy', type: 'birthdate', displayOrder: 4 },
          { key: 'phone_number', label: 'Phone Number', required: true, placeholder: 'Phone Number', type: 'phone_number', displayOrder: 5 },
          { label: 'Password', key: 'password', required: true, placeholder: 'Password', type: 'password', displayOrder: 6 }
        ]}
      />
    </AmplifyAuthenticator>
  )
}

export default App
